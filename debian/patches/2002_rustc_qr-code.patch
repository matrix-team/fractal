Description: avoid code requiring newer-than-in-Debian rustc
 This essentially reverts upstream git commit 1f2648f.
Author: Jonas Smedegaard <dr@jones.dk>
Forwarded: not-needed
Last-Update: 2024-01-23
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -21,6 +21,11 @@
 
 # Please keep dependencies sorted.
 [dependencies]
+ashpd = { version = "0.6", default-features = false, features = [
+    "pipewire",
+    "tracing",
+    "tokio",
+] }
 djb_hash = "0.1"
 eyeball-im = "0.4"
 futures-channel = "0.3"
@@ -35,6 +40,11 @@
 mime = "0.3"
 mime_guess = "2"
 once_cell = "1"
+oo7 = { version = "0.2", default-features = false, features = [
+    "native_crypto",
+    "tokio",
+    "tracing",
+] }
 pulldown-cmark = "0.9"
 qrcode = "0.12"
 rand = "0.8"
@@ -96,16 +106,3 @@
     "compat-get-3pids",
     "html",
 ]
-
-# Linux-only dependencies.
-[target.'cfg(target_os = "linux")'.dependencies]
-ashpd = { version = "0.6", default-features = false, features = [
-    "pipewire",
-    "tracing",
-    "tokio",
-] }
-oo7 = { version = "0.2", default-features = false, features = [
-    "native_crypto",
-    "tokio",
-    "tracing",
-] }
--- a/meson.build
+++ b/meson.build
@@ -39,15 +39,11 @@
   fallback: ['gtksourceview', 'gtksource_dep'],
   default_options: ['gtk_doc=false', 'sysprof=false', 'gir=false', 'vapi=false', 'install_tests=false']
 )
+dependency('libpipewire-0.3', version: '>= 0.3.0')
 dependency('openssl', version: '>= 1.0.1')
 dependency('shumate-1.0', version: '>= 1.0.0')
 dependency('sqlite3', version: '>= 3.24.0')
-
-# Linux-only dependencies
-if build_machine.system() == 'linux'
-  dependency('libpipewire-0.3', version: '>= 0.3.0')
-  dependency('xdg-desktop-portal', version: '>= 1.14.1')
-endif
+dependency('xdg-desktop-portal', version: '>= 1.14.1')
 
 glib_compile_resources = find_program('glib-compile-resources', required: true)
 glib_compile_schemas = find_program('glib-compile-schemas', required: true)
--- a/src/contrib/mod.rs
+++ b/src/contrib/mod.rs
@@ -3,5 +3,5 @@
 
 pub use self::{
     qr_code::QRCode,
-    qr_code_scanner::{Camera, CameraExt, QrCodeScanner},
+    qr_code_scanner::{Camera, QrCodeScanner},
 };
--- /dev/null
+++ b/src/contrib/qr_code_scanner/camera.rs
@@ -0,0 +1,123 @@
+// SPDX-License-Identifier: GPL-3.0-or-later
+use std::time::Duration;
+
+use ashpd::desktop::camera;
+use gtk::{glib, subclass::prelude::*};
+use once_cell::sync::Lazy;
+use tracing::error;
+
+use super::camera_paintable::CameraPaintable;
+use crate::{spawn_tokio, utils::timeout_future};
+
+mod imp {
+    use super::*;
+
+    #[derive(Debug, Default)]
+    pub struct Camera {
+        pub paintable: glib::WeakRef<CameraPaintable>,
+    }
+
+    #[glib::object_subclass]
+    impl ObjectSubclass for Camera {
+        const NAME: &'static str = "Camera";
+        type Type = super::Camera;
+    }
+
+    impl ObjectImpl for Camera {}
+}
+
+glib::wrapper! {
+    pub struct Camera(ObjectSubclass<imp::Camera>);
+}
+
+impl Camera {
+    /// Create a new `Camera`.
+    ///
+    /// Use `Camera::default()` to get a shared GObject.
+    fn new() -> Self {
+        glib::Object::new()
+    }
+
+    /// Ask the system whether cameras are available.
+    pub async fn has_cameras(&self) -> bool {
+        let handle = spawn_tokio!(async move {
+            let camera = match camera::Camera::new().await {
+                Ok(camera) => camera,
+                Err(error) => {
+                    error!("Failed to create instance of camera proxy: {error}");
+                    return false;
+                }
+            };
+
+            match camera.is_present().await {
+                Ok(is_present) => is_present,
+                Err(error) => {
+                    error!("Failed to check whether system has cameras: {error}");
+                    false
+                }
+            }
+        });
+        let abort_handle = handle.abort_handle();
+
+        match timeout_future(Duration::from_secs(1), handle).await {
+            Ok(is_present) => is_present.expect("The task should not have been aborted"),
+            Err(_) => {
+                abort_handle.abort();
+                error!("Failed to check whether system has cameras: the request timed out");
+                false
+            }
+        }
+    }
+
+    /// Get the a `gdk::Paintable` displaying the content of a camera.
+    ///
+    /// Panics if not called from the `MainContext` where GTK is running.
+    pub async fn paintable(&self) -> Option<CameraPaintable> {
+        // We need to make sure that the Paintable is taken only from the MainContext
+        assert!(glib::MainContext::default().is_owner());
+        let imp = self.imp();
+
+        if let Some(paintable) = imp.paintable.upgrade() {
+            return Some(paintable);
+        }
+
+        let handle = spawn_tokio!(async move { camera::request().await });
+        let abort_handle = handle.abort_handle();
+
+        match timeout_future(Duration::from_secs(1), handle).await {
+            Ok(tokio_res) => match tokio_res.expect("The task should not have been aborted") {
+                Ok(Some((fd, streams))) => {
+                    let paintable = CameraPaintable::new(fd, streams).await;
+                    imp.paintable.set(Some(&paintable));
+
+                    Some(paintable)
+                }
+                Ok(None) => {
+                    error!("Failed to request access to cameras: the response is empty");
+                    None
+                }
+                Err(error) => {
+                    error!("Failed to request access to cameras: {error}");
+                    None
+                }
+            },
+            Err(_) => {
+                // Error because we reached the timeout.
+                abort_handle.abort();
+                error!("Failed to request access to cameras: the request timed out");
+                None
+            }
+        }
+    }
+}
+
+impl Default for Camera {
+    fn default() -> Self {
+        static CAMERA: Lazy<Camera> = Lazy::new(Camera::new);
+
+        CAMERA.to_owned()
+    }
+}
+
+unsafe impl Send for Camera {}
+unsafe impl Sync for Camera {}
--- a/src/contrib/qr_code_scanner/camera/camera_paintable/mod.rs
+++ /dev/null
@@ -1,101 +0,0 @@
-/// Subclassable camera paintable.
-use gtk::{gdk, glib, glib::closure_local, prelude::*, subclass::prelude::*};
-use matrix_sdk::encryption::verification::QrVerificationData;
-
-#[cfg(target_os = "linux")]
-pub mod linux;
-
-use crate::contrib::qr_code_scanner::QrVerificationDataBoxed;
-
-pub enum Action {
-    QrCodeDetected(QrVerificationData),
-}
-
-mod imp {
-    use glib::subclass::Signal;
-    use once_cell::sync::Lazy;
-
-    use super::*;
-
-    #[repr(C)]
-    pub struct CameraPaintableClass {
-        pub parent_class: glib::object::Class<glib::Object>,
-    }
-
-    unsafe impl ClassStruct for CameraPaintableClass {
-        type Type = CameraPaintable;
-    }
-
-    #[derive(Debug, Default)]
-    pub struct CameraPaintable;
-
-    #[glib::object_subclass]
-    impl ObjectSubclass for CameraPaintable {
-        const NAME: &'static str = "CameraPaintable";
-        type Type = super::CameraPaintable;
-        type Class = CameraPaintableClass;
-        type Interfaces = (gdk::Paintable,);
-    }
-
-    impl ObjectImpl for CameraPaintable {
-        fn signals() -> &'static [Signal] {
-            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
-                vec![Signal::builder("code-detected")
-                    .param_types([QrVerificationDataBoxed::static_type()])
-                    .run_first()
-                    .build()]
-            });
-            SIGNALS.as_ref()
-        }
-    }
-
-    impl PaintableImpl for CameraPaintable {
-        fn snapshot(&self, _snapshot: &gdk::Snapshot, _width: f64, _height: f64) {
-            // Nothing to do
-        }
-    }
-}
-
-glib::wrapper! {
-    /// A subclassable paintable to display the output of a camera.
-    pub struct CameraPaintable(ObjectSubclass<imp::CameraPaintable>)
-        @implements gdk::Paintable;
-}
-
-pub trait CameraPaintableExt: 'static {
-    /// Connect to the signal emitted when a code is detected.
-    fn connect_code_detected<F: Fn(&Self, QrVerificationData) + 'static>(
-        &self,
-        f: F,
-    ) -> glib::SignalHandlerId;
-}
-
-impl<O: IsA<CameraPaintable>> CameraPaintableExt for O {
-    fn connect_code_detected<F: Fn(&Self, QrVerificationData) + 'static>(
-        &self,
-        f: F,
-    ) -> glib::SignalHandlerId {
-        self.connect_closure(
-            "activated",
-            true,
-            closure_local!(move |obj: Self, data: QrVerificationDataBoxed| {
-                f(&obj, data.0);
-            }),
-        )
-    }
-}
-
-/// Public trait that must be implemented for everything that derives from
-/// `CameraPaintable`.
-///
-/// Overriding a method from this Trait overrides also its behavior in
-/// `CameraPaintableExt`.
-#[allow(async_fn_in_trait)]
-pub trait CameraPaintableImpl: ObjectImpl + PaintableImpl {}
-
-unsafe impl<T> IsSubclassable<T> for CameraPaintable
-where
-    T: CameraPaintableImpl,
-    T::Type: IsA<CameraPaintable>,
-{
-}
--- a/src/contrib/qr_code_scanner/camera/linux.rs
+++ /dev/null
@@ -1,119 +0,0 @@
-// SPDX-License-Identifier: GPL-3.0-or-later
-use std::time::Duration;
-
-use ashpd::desktop::camera;
-use gtk::{glib, prelude::*, subclass::prelude::*};
-use tracing::error;
-
-use super::{
-    camera_paintable::{linux::LinuxCameraPaintable, CameraPaintable},
-    Camera, CameraImpl,
-};
-use crate::{spawn_tokio, utils::timeout_future};
-
-mod imp {
-    use super::*;
-
-    #[derive(Debug, Default)]
-    pub struct LinuxCamera {
-        pub paintable: glib::WeakRef<LinuxCameraPaintable>,
-    }
-
-    #[glib::object_subclass]
-    impl ObjectSubclass for LinuxCamera {
-        const NAME: &'static str = "LinuxCamera";
-        type Type = super::LinuxCamera;
-        type ParentType = Camera;
-    }
-
-    impl ObjectImpl for LinuxCamera {}
-
-    impl CameraImpl for LinuxCamera {
-        async fn has_cameras(&self) -> bool {
-            let handle = spawn_tokio!(async move {
-                let camera = match camera::Camera::new().await {
-                    Ok(camera) => camera,
-                    Err(error) => {
-                        error!("Failed to create instance of camera proxy: {error}");
-                        return false;
-                    }
-                };
-
-                match camera.is_present().await {
-                    Ok(is_present) => is_present,
-                    Err(error) => {
-                        error!("Failed to check whether system has cameras: {error}");
-                        false
-                    }
-                }
-            });
-            let abort_handle = handle.abort_handle();
-
-            match timeout_future(Duration::from_secs(1), handle).await {
-                Ok(is_present) => is_present.expect("The task should not have been aborted"),
-                Err(_) => {
-                    abort_handle.abort();
-                    error!("Failed to check whether system has cameras: the request timed out");
-                    false
-                }
-            }
-        }
-
-        async fn paintable(&self) -> Option<CameraPaintable> {
-            // We need to make sure that the Paintable is taken only from the MainContext
-            assert!(glib::MainContext::default().is_owner());
-
-            if let Some(paintable) = self.paintable.upgrade() {
-                return Some(paintable.upcast());
-            }
-
-            let handle = spawn_tokio!(async move { camera::request().await });
-            let abort_handle = handle.abort_handle();
-
-            match timeout_future(Duration::from_secs(1), handle).await {
-                Ok(tokio_res) => match tokio_res.expect("The task should not have been aborted") {
-                    Ok(Some((fd, streams))) => {
-                        let paintable = LinuxCameraPaintable::new(fd, streams).await;
-                        self.paintable.set(Some(&paintable));
-
-                        Some(paintable.upcast())
-                    }
-                    Ok(None) => {
-                        error!("Failed to request access to cameras: the response is empty");
-                        None
-                    }
-                    Err(error) => {
-                        error!("Failed to request access to cameras: {error}");
-                        None
-                    }
-                },
-                Err(_) => {
-                    // Error because we reached the timeout.
-                    abort_handle.abort();
-                    error!("Failed to request access to cameras: the request timed out");
-                    None
-                }
-            }
-        }
-    }
-}
-
-glib::wrapper! {
-    pub struct LinuxCamera(ObjectSubclass<imp::LinuxCamera>) @extends Camera;
-}
-
-impl LinuxCamera {
-    /// Create a new `LinuxCamera`.
-    pub fn new() -> Self {
-        glib::Object::new()
-    }
-}
-
-impl Default for LinuxCamera {
-    fn default() -> Self {
-        Self::new()
-    }
-}
-
-unsafe impl Send for LinuxCamera {}
-unsafe impl Sync for LinuxCamera {}
--- a/src/contrib/qr_code_scanner/camera/mod.rs
+++ /dev/null
@@ -1,155 +0,0 @@
-//! Camera API.
-
-use futures_util::{future::LocalBoxFuture, FutureExt};
-use gtk::{glib, prelude::*, subclass::prelude::*};
-use once_cell::sync::Lazy;
-use tracing::error;
-
-mod camera_paintable;
-#[cfg(target_os = "linux")]
-mod linux;
-
-pub use self::camera_paintable::Action;
-use self::camera_paintable::CameraPaintable;
-
-mod imp {
-
-    use super::*;
-
-    #[repr(C)]
-    pub struct CameraClass {
-        pub parent_class: glib::object::Class<glib::Object>,
-        pub has_cameras: fn(&super::Camera) -> LocalBoxFuture<bool>,
-        pub paintable: fn(&super::Camera) -> LocalBoxFuture<Option<CameraPaintable>>,
-    }
-
-    unsafe impl ClassStruct for CameraClass {
-        type Type = Camera;
-    }
-
-    pub(super) async fn camera_has_cameras(this: &super::Camera) -> bool {
-        let klass = this.class();
-        (klass.as_ref().has_cameras)(this).await
-    }
-
-    pub(super) async fn camera_paintable(this: &super::Camera) -> Option<CameraPaintable> {
-        let klass = this.class();
-        (klass.as_ref().paintable)(this).await
-    }
-
-    #[derive(Debug, Default)]
-    pub struct Camera;
-
-    #[glib::object_subclass]
-    impl ObjectSubclass for Camera {
-        const NAME: &'static str = "Camera";
-        type Type = super::Camera;
-        type Class = CameraClass;
-    }
-
-    impl ObjectImpl for Camera {}
-}
-
-glib::wrapper! {
-    /// Subclassable Camera API.
-    ///
-    /// The default implementation, for unsupported platforms, makes sure the camera support is disabled.
-    pub struct Camera(ObjectSubclass<imp::Camera>);
-}
-
-impl Camera {
-    /// Create a new `Camera`.
-    ///
-    /// Use `Camera::default()` to get a shared GObject.
-    fn new() -> Self {
-        #[cfg(target_os = "linux")]
-        let obj = linux::LinuxCamera::new().upcast();
-
-        #[cfg(not(target_os = "linux"))]
-        let obj = glib::Object::new();
-
-        obj
-    }
-}
-
-impl Default for Camera {
-    fn default() -> Self {
-        static CAMERA: Lazy<Camera> = Lazy::new(Camera::new);
-
-        CAMERA.to_owned()
-    }
-}
-
-unsafe impl Send for Camera {}
-unsafe impl Sync for Camera {}
-
-pub trait CameraExt: 'static {
-    /// Whether any cameras are available.
-    async fn has_cameras(&self) -> bool;
-
-    /// The paintable displaying the camera.
-    async fn paintable(&self) -> Option<CameraPaintable>;
-}
-
-impl<O: IsA<Camera>> CameraExt for O {
-    async fn has_cameras(&self) -> bool {
-        imp::camera_has_cameras(self.upcast_ref()).await
-    }
-
-    async fn paintable(&self) -> Option<CameraPaintable> {
-        imp::camera_paintable(self.upcast_ref()).await
-    }
-}
-
-/// Public trait that must be implemented for everything that derives from
-/// `Camera`.
-///
-/// Overriding a method from this Trait overrides also its behavior in
-/// `CameraExt`.
-#[allow(async_fn_in_trait)]
-pub trait CameraImpl: ObjectImpl {
-    /// Whether any cameras are available.
-    async fn has_cameras(&self) -> bool {
-        false
-    }
-
-    /// The paintable displaying the camera.
-    async fn paintable(&self) -> Option<CameraPaintable> {
-        error!("The camera API is not supported on this platform");
-        None
-    }
-}
-
-unsafe impl<T> IsSubclassable<T> for Camera
-where
-    T: CameraImpl,
-    T::Type: IsA<Camera>,
-{
-    fn class_init(class: &mut glib::Class<Self>) {
-        Self::parent_class_init::<T>(class.upcast_ref_mut());
-
-        let klass = class.as_mut();
-
-        klass.has_cameras = has_cameras_trampoline::<T>;
-        klass.paintable = paintable_trampoline::<T>;
-    }
-}
-
-// Virtual method implementation trampolines.
-fn has_cameras_trampoline<T>(this: &Camera) -> LocalBoxFuture<bool>
-where
-    T: ObjectSubclass + CameraImpl,
-    T::Type: IsA<Camera>,
-{
-    let this = this.downcast_ref::<T::Type>().unwrap();
-    this.imp().has_cameras().boxed_local()
-}
-
-fn paintable_trampoline<T>(this: &Camera) -> LocalBoxFuture<Option<CameraPaintable>>
-where
-    T: ObjectSubclass + CameraImpl,
-    T::Type: IsA<Camera>,
-{
-    let this = this.downcast_ref::<T::Type>().unwrap();
-    this.imp().paintable().boxed_local()
-}
--- a/src/contrib/qr_code_scanner/camera/camera_paintable/linux.rs
+++ /dev/null
@@ -1,268 +0,0 @@
-// SPDX-License-Identifier: GPL-3.0-or-later
-//
-// Fancy Camera with QR code detection
-//
-// Pipeline:
-//                            queue -- videoconvert -- QrCodeDetector sink
-//                         /
-//     pipewiresrc -- tee
-//                         \
-//                            queue -- videoconvert -- gst paintable sink
-
-use std::{
-    cell::Cell,
-    os::unix::io::AsRawFd,
-    sync::{Arc, Mutex},
-};
-
-use ashpd::desktop::camera;
-use gst::{bus::BusWatchGuard, prelude::*};
-use gtk::{
-    gdk, glib,
-    glib::{clone, subclass::prelude::*},
-    graphene,
-    prelude::*,
-    subclass::prelude::*,
-};
-use tracing::{debug, error};
-
-use super::{Action, CameraPaintable, CameraPaintableImpl};
-use crate::contrib::qr_code_scanner::{qr_code_detector::QrCodeDetector, QrVerificationDataBoxed};
-
-mod imp {
-    use std::cell::RefCell;
-
-    use super::*;
-
-    #[derive(Debug, Default)]
-    pub struct LinuxCameraPaintable {
-        pub pipeline: RefCell<Option<(gst::Pipeline, BusWatchGuard)>>,
-        pub sink_paintable: RefCell<Option<gdk::Paintable>>,
-    }
-
-    #[glib::object_subclass]
-    impl ObjectSubclass for LinuxCameraPaintable {
-        const NAME: &'static str = "LinuxCameraPaintable";
-        type Type = super::LinuxCameraPaintable;
-        type ParentType = CameraPaintable;
-        type Interfaces = (gdk::Paintable,);
-    }
-
-    impl ObjectImpl for LinuxCameraPaintable {
-        fn dispose(&self) {
-            self.obj().set_pipeline(None);
-        }
-    }
-
-    impl CameraPaintableImpl for LinuxCameraPaintable {}
-
-    impl PaintableImpl for LinuxCameraPaintable {
-        fn intrinsic_height(&self) -> i32 {
-            if let Some(paintable) = self.sink_paintable.borrow().as_ref() {
-                paintable.intrinsic_height()
-            } else {
-                0
-            }
-        }
-
-        fn intrinsic_width(&self) -> i32 {
-            if let Some(paintable) = self.sink_paintable.borrow().as_ref() {
-                paintable.intrinsic_width()
-            } else {
-                0
-            }
-        }
-
-        fn snapshot(&self, snapshot: &gdk::Snapshot, width: f64, height: f64) {
-            let snapshot = snapshot.downcast_ref::<gtk::Snapshot>().unwrap();
-
-            let paintable = self.sink_paintable.borrow();
-            let Some(image) = paintable.as_ref() else {
-                return;
-            };
-
-            // Transformation to avoid stretching the camera. We translate and scale the
-            // image.
-            let aspect = width / height.max(std::f64::EPSILON); // Do not divide by zero.
-            let image_aspect = image.intrinsic_aspect_ratio();
-
-            if image_aspect == 0.0 {
-                image.snapshot(snapshot, width, height);
-                return;
-            };
-
-            let (new_width, new_height) = match aspect <= image_aspect {
-                true => (height * image_aspect, height), // Mobile view
-                false => (width, width / image_aspect),  // Landscape
-            };
-
-            let p = graphene::Point::new(
-                ((width - new_width) / 2.0) as f32,
-                ((height - new_height) / 2.0) as f32,
-            );
-            snapshot.translate(&p);
-
-            image.snapshot(snapshot, new_width, new_height);
-        }
-    }
-}
-
-glib::wrapper! {
-    /// A paintable to display the output of a camera on Linux.
-    pub struct LinuxCameraPaintable(ObjectSubclass<imp::LinuxCameraPaintable>)
-        @extends CameraPaintable, @implements gdk::Paintable;
-}
-
-impl LinuxCameraPaintable {
-    pub async fn new<F: AsRawFd>(fd: F, streams: Vec<camera::Stream>) -> Self {
-        let self_: Self = glib::Object::new();
-
-        self_.set_pipewire_fd(fd, streams).await;
-        self_
-    }
-
-    async fn set_pipewire_fd<F: AsRawFd>(&self, fd: F, streams: Vec<camera::Stream>) {
-        // Make sure that the previous pipeline is closed so that we can be sure that it
-        // doesn't use the webcam
-        self.set_pipeline(None);
-
-        let mut src_builder =
-            gst::ElementFactory::make("pipewiresrc").property("fd", fd.as_raw_fd());
-        if let Some(node_id) = streams.first().map(|s| s.node_id()) {
-            src_builder = src_builder.property("path", node_id.to_string());
-        }
-        let pipewire_src = src_builder.build().unwrap();
-
-        let pipeline = gst::Pipeline::new();
-        let detector = QrCodeDetector::new(self.create_sender()).upcast();
-
-        let tee = gst::ElementFactory::make("tee").build().unwrap();
-        let queue = gst::ElementFactory::make("queue").build().unwrap();
-        let videoconvert1 = gst::ElementFactory::make("videoconvert").build().unwrap();
-        let videoconvert2 = gst::ElementFactory::make("videoconvert").build().unwrap();
-        let src_pad = queue.static_pad("src").unwrap();
-
-        // Reduce the number of frames we use to get the qrcode from
-        let start = Arc::new(Mutex::new(std::time::Instant::now()));
-        src_pad.add_probe(gst::PadProbeType::BUFFER, move |_, _| {
-            let mut start = start.lock().unwrap();
-            if start.elapsed() < std::time::Duration::from_millis(500) {
-                gst::PadProbeReturn::Drop
-            } else {
-                *start = std::time::Instant::now();
-                gst::PadProbeReturn::Ok
-            }
-        });
-
-        let queue2 = gst::ElementFactory::make("queue").build().unwrap();
-        let sink = gst::ElementFactory::make("gtk4paintablesink")
-            .build()
-            .unwrap();
-
-        pipeline
-            .add_many([
-                &pipewire_src,
-                &tee,
-                &queue,
-                &videoconvert1,
-                &detector,
-                &queue2,
-                &videoconvert2,
-                &sink,
-            ])
-            .unwrap();
-
-        gst::Element::link_many([&pipewire_src, &tee, &queue, &videoconvert1, &detector]).unwrap();
-
-        tee.link_pads(None, &queue2, None).unwrap();
-        gst::Element::link_many([&queue2, &videoconvert2, &sink]).unwrap();
-
-        let bus = pipeline.bus().unwrap();
-        let bus_guard = bus.add_watch_local(
-            clone!(@weak self as paintable => @default-return glib::ControlFlow::Break, move |_, msg| {
-                if let gst::MessageView::Error(err) = msg.view() {
-                    error!(
-                        "Error from {:?}: {} ({:?})",
-                        err.src().map(|s| s.path_string()),
-                        err.error(),
-                        err.debug()
-                    );
-                }
-                glib::ControlFlow::Continue
-            }),
-        )
-        .expect("Failed to add bus watch");
-
-        let paintable = sink.property::<gdk::Paintable>("paintable");
-
-        // Workaround: we wait for the first frame so that we don't show a black frame
-        let (sender, receiver) = futures_channel::oneshot::channel();
-        let sender = Cell::new(Some(sender));
-
-        paintable.connect_invalidate_contents(move |_| {
-            if let Some(sender) = sender.take() {
-                if sender.send(()).is_err() {
-                    error!("Failed to send camera paintable `invalidate-contents` signal");
-                }
-            }
-        });
-
-        self.set_sink_paintable(paintable);
-        pipeline.set_state(gst::State::Playing).unwrap();
-        self.set_pipeline(Some((pipeline, bus_guard)));
-
-        if receiver.await.is_err() {
-            debug!("Camera paintable `invalidate-contents` signal sender was dropped");
-        }
-    }
-
-    fn set_sink_paintable(&self, paintable: gdk::Paintable) {
-        let imp = self.imp();
-
-        paintable.connect_invalidate_contents(clone!(@weak self as obj => move |_| {
-            obj.invalidate_contents();
-        }));
-
-        paintable.connect_invalidate_size(clone!(@weak self as obj => move |_| {
-            obj.invalidate_size();
-        }));
-
-        imp.sink_paintable.replace(Some(paintable));
-
-        self.invalidate_contents();
-        self.invalidate_size();
-    }
-
-    fn set_pipeline(&self, pipeline: Option<(gst::Pipeline, BusWatchGuard)>) {
-        let imp = self.imp();
-
-        if let Some((pipeline, _)) = imp.pipeline.take() {
-            pipeline.set_state(gst::State::Null).unwrap();
-        }
-
-        if pipeline.is_none() {
-            return;
-        }
-
-        imp.pipeline.replace(pipeline);
-    }
-
-    fn create_sender(&self) -> glib::Sender<Action> {
-        #[allow(deprecated)]
-        let (sender, receiver) = glib::MainContext::channel(glib::Priority::DEFAULT);
-
-        receiver.attach(
-            None,
-            glib::clone!(@weak self as obj => @default-return glib::ControlFlow::Break, move |action| {
-                match action {
-                    Action::QrCodeDetected(code) => {
-                        obj.emit_by_name::<()>("code-detected", &[&QrVerificationDataBoxed(code)]);
-                    }
-                }
-                glib::ControlFlow::Continue
-            }),
-        );
-
-        sender
-    }
-}
--- /dev/null
+++ b/src/contrib/qr_code_scanner/camera_paintable.rs
@@ -0,0 +1,280 @@
+// SPDX-License-Identifier: GPL-3.0-or-later
+//
+// Fancy Camera with QR code detection
+//
+// Pipeline:
+//                            queue -- videoconvert -- QrCodeDetector sink
+//                         /
+//     pipewiresrc -- tee
+//                         \
+//                            queue -- videoconvert -- gst paintable sink
+
+use std::{
+    cell::Cell,
+    os::unix::io::AsRawFd,
+    sync::{Arc, Mutex},
+};
+
+use ashpd::desktop::camera;
+use gst::{bus::BusWatchGuard, prelude::*};
+use gtk::{
+    gdk, glib,
+    glib::{clone, subclass::prelude::*},
+    graphene,
+    prelude::*,
+    subclass::prelude::*,
+};
+use matrix_sdk::encryption::verification::QrVerificationData;
+use tracing::{debug, error};
+
+use crate::contrib::qr_code_scanner::{qr_code_detector::QrCodeDetector, QrVerificationDataBoxed};
+
+pub enum Action {
+    QrCodeDetected(QrVerificationData),
+}
+
+mod imp {
+    use std::cell::RefCell;
+
+    use glib::subclass;
+    use once_cell::sync::Lazy;
+
+    use super::*;
+
+    #[derive(Debug, Default)]
+    pub struct CameraPaintable {
+        pub pipeline: RefCell<Option<(gst::Pipeline, BusWatchGuard)>>,
+        pub sink_paintable: RefCell<Option<gdk::Paintable>>,
+    }
+
+    #[glib::object_subclass]
+    impl ObjectSubclass for CameraPaintable {
+        const NAME: &'static str = "CameraPaintable";
+        type Type = super::CameraPaintable;
+        type Interfaces = (gdk::Paintable,);
+    }
+
+    impl ObjectImpl for CameraPaintable {
+        fn dispose(&self) {
+            self.obj().set_pipeline(None);
+        }
+
+        fn signals() -> &'static [subclass::Signal] {
+            static SIGNALS: Lazy<Vec<subclass::Signal>> = Lazy::new(|| {
+                vec![subclass::Signal::builder("code-detected")
+                    .param_types([QrVerificationDataBoxed::static_type()])
+                    .run_first()
+                    .build()]
+            });
+            SIGNALS.as_ref()
+        }
+    }
+
+    impl PaintableImpl for CameraPaintable {
+        fn intrinsic_height(&self) -> i32 {
+            if let Some(paintable) = self.sink_paintable.borrow().as_ref() {
+                paintable.intrinsic_height()
+            } else {
+                0
+            }
+        }
+
+        fn intrinsic_width(&self) -> i32 {
+            if let Some(paintable) = self.sink_paintable.borrow().as_ref() {
+                paintable.intrinsic_width()
+            } else {
+                0
+            }
+        }
+
+        fn snapshot(&self, snapshot: &gdk::Snapshot, width: f64, height: f64) {
+            let snapshot = snapshot.downcast_ref::<gtk::Snapshot>().unwrap();
+
+            let paintable = self.sink_paintable.borrow();
+            let Some(image) = paintable.as_ref() else {
+                return;
+            };
+
+            // Transformation to avoid stretching the camera. We translate and scale the
+            // image.
+            let aspect = width / height.max(std::f64::EPSILON); // Do not divide by zero.
+            let image_aspect = image.intrinsic_aspect_ratio();
+
+            if image_aspect == 0.0 {
+                image.snapshot(snapshot, width, height);
+                return;
+            };
+
+            let (new_width, new_height) = match aspect <= image_aspect {
+                true => (height * image_aspect, height), // Mobile view
+                false => (width, width / image_aspect),  // Landscape
+            };
+
+            let p = graphene::Point::new(
+                ((width - new_width) / 2.0) as f32,
+                ((height - new_height) / 2.0) as f32,
+            );
+            snapshot.translate(&p);
+
+            image.snapshot(snapshot, new_width, new_height);
+        }
+    }
+}
+
+glib::wrapper! {
+    pub struct CameraPaintable(ObjectSubclass<imp::CameraPaintable>) @implements gdk::Paintable;
+}
+
+impl CameraPaintable {
+    pub async fn new<F: AsRawFd>(fd: F, streams: Vec<camera::Stream>) -> Self {
+        let self_: Self = glib::Object::new();
+
+        self_.set_pipewire_fd(fd, streams).await;
+        self_
+    }
+
+    async fn set_pipewire_fd<F: AsRawFd>(&self, fd: F, streams: Vec<camera::Stream>) {
+        // Make sure that the previous pipeline is closed so that we can be sure that it
+        // doesn't use the webcam
+        self.set_pipeline(None);
+
+        let mut src_builder =
+            gst::ElementFactory::make("pipewiresrc").property("fd", fd.as_raw_fd());
+        if let Some(node_id) = streams.first().map(|s| s.node_id()) {
+            src_builder = src_builder.property("path", node_id.to_string());
+        }
+        let pipewire_src = src_builder.build().unwrap();
+
+        let pipeline = gst::Pipeline::new();
+        let detector = QrCodeDetector::new(self.create_sender()).upcast();
+
+        let tee = gst::ElementFactory::make("tee").build().unwrap();
+        let queue = gst::ElementFactory::make("queue").build().unwrap();
+        let videoconvert1 = gst::ElementFactory::make("videoconvert").build().unwrap();
+        let videoconvert2 = gst::ElementFactory::make("videoconvert").build().unwrap();
+        let src_pad = queue.static_pad("src").unwrap();
+
+        // Reduce the number of frames we use to get the qrcode from
+        let start = Arc::new(Mutex::new(std::time::Instant::now()));
+        src_pad.add_probe(gst::PadProbeType::BUFFER, move |_, _| {
+            let mut start = start.lock().unwrap();
+            if start.elapsed() < std::time::Duration::from_millis(500) {
+                gst::PadProbeReturn::Drop
+            } else {
+                *start = std::time::Instant::now();
+                gst::PadProbeReturn::Ok
+            }
+        });
+
+        let queue2 = gst::ElementFactory::make("queue").build().unwrap();
+        let sink = gst::ElementFactory::make("gtk4paintablesink")
+            .build()
+            .unwrap();
+
+        pipeline
+            .add_many([
+                &pipewire_src,
+                &tee,
+                &queue,
+                &videoconvert1,
+                &detector,
+                &queue2,
+                &videoconvert2,
+                &sink,
+            ])
+            .unwrap();
+
+        gst::Element::link_many([&pipewire_src, &tee, &queue, &videoconvert1, &detector]).unwrap();
+
+        tee.link_pads(None, &queue2, None).unwrap();
+        gst::Element::link_many([&queue2, &videoconvert2, &sink]).unwrap();
+
+        let bus = pipeline.bus().unwrap();
+        let bus_guard = bus.add_watch_local(
+            clone!(@weak self as paintable => @default-return glib::ControlFlow::Break, move |_, msg| {
+                if let gst::MessageView::Error(err) = msg.view() {
+                    error!(
+                        "Error from {:?}: {} ({:?})",
+                        err.src().map(|s| s.path_string()),
+                        err.error(),
+                        err.debug()
+                    );
+                }
+                glib::ControlFlow::Continue
+            }),
+        )
+        .expect("Failed to add bus watch");
+
+        let paintable = sink.property::<gdk::Paintable>("paintable");
+
+        // Workaround: we wait for the first frame so that we don't show a black frame
+        let (sender, receiver) = futures_channel::oneshot::channel();
+        let sender = Cell::new(Some(sender));
+
+        paintable.connect_invalidate_contents(move |_| {
+            if let Some(sender) = sender.take() {
+                if sender.send(()).is_err() {
+                    error!("Failed to send camera paintable `invalidate-contents` signal");
+                }
+            }
+        });
+
+        self.set_sink_paintable(paintable);
+        pipeline.set_state(gst::State::Playing).unwrap();
+        self.set_pipeline(Some((pipeline, bus_guard)));
+
+        if receiver.await.is_err() {
+            debug!("Camera paintable `invalidate-contents` signal sender was dropped");
+        }
+    }
+
+    fn set_sink_paintable(&self, paintable: gdk::Paintable) {
+        let imp = self.imp();
+
+        paintable.connect_invalidate_contents(clone!(@weak self as obj => move |_| {
+            obj.invalidate_contents();
+        }));
+
+        paintable.connect_invalidate_size(clone!(@weak self as obj => move |_| {
+            obj.invalidate_size();
+        }));
+
+        imp.sink_paintable.replace(Some(paintable));
+
+        self.invalidate_contents();
+        self.invalidate_size();
+    }
+
+    fn set_pipeline(&self, pipeline: Option<(gst::Pipeline, BusWatchGuard)>) {
+        let imp = self.imp();
+
+        if let Some((pipeline, _)) = imp.pipeline.take() {
+            pipeline.set_state(gst::State::Null).unwrap();
+        }
+
+        if pipeline.is_none() {
+            return;
+        }
+
+        imp.pipeline.replace(pipeline);
+    }
+
+    fn create_sender(&self) -> glib::Sender<Action> {
+        #[allow(deprecated)]
+        let (sender, receiver) = glib::MainContext::channel(glib::Priority::DEFAULT);
+
+        receiver.attach(
+            None,
+            glib::clone!(@weak self as obj => @default-return glib::ControlFlow::Break, move |action| {
+                match action {
+                    Action::QrCodeDetected(code) => {
+                        obj.emit_by_name::<()>("code-detected", &[&QrVerificationDataBoxed(code)]);
+                    }
+                }
+                glib::ControlFlow::Continue
+            }),
+        );
+
+        sender
+    }
+}
--- a/src/contrib/qr_code_scanner/mod.rs
+++ b/src/contrib/qr_code_scanner/mod.rs
@@ -3,9 +3,10 @@
 use matrix_sdk::encryption::verification::QrVerificationData;
 
 mod camera;
+mod camera_paintable;
 mod qr_code_detector;
 
-pub use camera::{Camera, CameraExt};
+pub use camera::Camera;
 
 mod imp {
     use std::cell::RefCell;
--- a/src/contrib/qr_code_scanner/qr_code_detector.rs
+++ b/src/contrib/qr_code_scanner/qr_code_detector.rs
@@ -8,7 +8,7 @@
 use tracing::debug;
 
 use super::*;
-use crate::contrib::qr_code_scanner::camera::Action;
+use crate::contrib::qr_code_scanner::camera_paintable::Action;
 
 const HEADER: &[u8] = b"MATRIX";
 
--- a/src/prelude.rs
+++ b/src/prelude.rs
@@ -1,6 +1,5 @@
 pub use crate::{
     components::{ToastableWindowExt, ToastableWindowImpl},
-    contrib::CameraExt,
     session::model::{TimelineItemExt, UserExt},
     session_list::SessionInfoExt,
     system_settings::SystemSettingsExt,
--- a/src/session/model/verification/mod.rs
+++ b/src/session/model/verification/mod.rs
@@ -10,7 +10,7 @@
     },
     verification_list::VerificationList,
 };
-use crate::{contrib::Camera, prelude::*};
+use crate::contrib::Camera;
 
 /// A unique key to identify an identity verification.
 #[derive(Debug, Clone, Hash, PartialEq, Eq)]
