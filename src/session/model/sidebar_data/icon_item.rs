use std::fmt;

use gettextrs::gettext;
use gtk::{glib, prelude::*, subclass::prelude::*};

use super::{CategoryType, SidebarItem, SidebarItemExt, SidebarItemImpl};

#[derive(Debug, Default, Hash, Eq, PartialEq, Clone, Copy, glib::Enum)]
#[repr(u32)]
#[enum_type(name = "SidebarIconItemType")]
pub enum SidebarIconItemType {
    #[default]
    Explore = 0,
    Forget = 1,
}

impl SidebarIconItemType {
    /// The icon name for this item type.
    pub fn icon_name(&self) -> &'static str {
        match self {
            Self::Explore => "explore-symbolic",
            Self::Forget => "user-trash-symbolic",
        }
    }
}

impl fmt::Display for SidebarIconItemType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let label = match self {
            Self::Explore => gettext("Explore"),
            Self::Forget => gettext("Forget Room"),
        };

        f.write_str(&label)
    }
}

mod imp {
    use std::{cell::Cell, marker::PhantomData};

    use super::*;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::SidebarIconItem)]
    pub struct SidebarIconItem {
        /// The type of this item.
        #[property(get, construct_only, builder(SidebarIconItemType::default()))]
        pub item_type: Cell<SidebarIconItemType>,
        /// The display name of this item.
        #[property(get = Self::display_name)]
        pub display_name: PhantomData<String>,
        /// The icon name used for this item.
        #[property(get = Self::icon_name)]
        pub icon_name: PhantomData<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SidebarIconItem {
        const NAME: &'static str = "SidebarIconItem";
        type Type = super::SidebarIconItem;
        type ParentType = SidebarItem;
    }

    #[glib::derived_properties]
    impl ObjectImpl for SidebarIconItem {}

    impl SidebarItemImpl for SidebarIconItem {
        fn update_visibility(&self, for_category: CategoryType) {
            let obj = self.obj();

            match self.item_type.get() {
                SidebarIconItemType::Explore => obj.set_visible(true),
                SidebarIconItemType::Forget => obj.set_visible(for_category == CategoryType::Left),
            }
        }
    }

    impl SidebarIconItem {
        /// The display name of this item.
        fn display_name(&self) -> String {
            self.item_type.get().to_string()
        }

        /// The icon name used for this item.
        fn icon_name(&self) -> String {
            self.item_type.get().icon_name().to_owned()
        }
    }
}

glib::wrapper! {
    /// A top-level row in the sidebar with an icon.
    pub struct SidebarIconItem(ObjectSubclass<imp::SidebarIconItem>) @extends SidebarItem;
}

impl SidebarIconItem {
    pub fn new(item_type: SidebarIconItemType) -> Self {
        glib::Object::builder()
            .property("item-type", item_type)
            .build()
    }
}
