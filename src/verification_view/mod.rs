mod identity_verification_view;
mod session_verification_view;

pub use self::{
    identity_verification_view::IdentityVerificationView,
    session_verification_view::SessionVerificationView,
};
